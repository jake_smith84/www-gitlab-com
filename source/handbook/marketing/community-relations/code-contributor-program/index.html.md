---
layout: markdown_page
title: "Code Contributor Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----
## Communication/collaboration with contributors

### Contributor channel

A GitLab community room is available on [Gitter](https://gitter.im/gitlabhq/community) for people interested in contributing to GitLab.  This is open to everyone to join.

### Hackathons

There will be a quarterly [Hackathon](https://about.gitlab.com/community/hackathon/) for GitLab community members to come together to work on merge requests, participate in tutorial sessions, and support each other on the [GitLab community channel](https://gitter.im/gitlabhq/community).  Agenda, logistics, materials, recordings, and other information for Hackathons will be available on the [Hackathon project pages](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-hackathon).

The event planning will be done following the [Hackathon checklist](/handbook/marketing/community-relations/code-contributor-program/templates/hackathon-checklist.html)

## Supporting the contributor community

### Mentors for new contributors
After the first merged MR, make an offer using an [outreach email template](/handbook/marketing/community-relations/code-contributor-program/templates/email-templates.html) to pair the new contributor with an experienced mentor from the community.  This is for a limited time period only (2 weeks) so that mentors are not overly burdened.  

Currently, the mentors consist primarily of [Core Team members](https://about.gitlab.com/core-team/).  

### Working with the Core Team
There will be a monthly call with members of the [Core Team](https://about.gitlab.com/core-team/) using the Zoom conferencing tool and meetings will also be recorded. Since Core Team members are spread out in different time zones, meeting times will be rotated.   

[Service Desk](https://gitlab.com/gitlab-core-team/general/issues/service_desk) will be used as a communication tool for Core Team members. Anyone can open issues in the Service Desk or send an email to `incoming+gitlab-core-team/general@incoming.gitlab.com`  Core Team members who signed NDAs will also have access to GitLab Slack channels.  

### For contributors to the GitLab Enterprise Edition (EE)

In order for community contributors to contribute to the [GitLab Enterprise Edition](https://gitlab.com/gitlab-org/gitlab-ee), they will need a license for EE.  If they don't already have a license, they can get a [free trial for 30 days](https://about.gitlab.com/free-trial/).  If the contributor is not able to complete their work in 30 days, issue a new EE license for a 3-month period for a limited number of users. If the contributor wants to continue contributing to EE beyond the 3 months (and there has been contributions during the 3-month period), we can renew the license for another 3-month period.  

## Community outreach

### Contributor blog post series

Goal is to publish a regular blog post featuring contributors from the community.  The format will be a [casual Q&A with a community member](https://about.gitlab.com/2018/08/08/contributor-post-vitaliy/) and will be posted on the [GitLab blog page](https://about.gitlab.com/blog/).

When developing a blog post, follow the [blog guidelines](/handbook/marketing/blog/).

### Following up on inactive merge requests

Periodically, there should be a review of community MR's that has not had a meaningful activity for an extended period (e.g. >3 months).  A good place to start would be to search for all MR's with a label `Community contribution` and a milestone `No milestone`.  A follow-up should be made with either the community member or MR coaches to ask if there's a plan to continue work on the MR or if MR should be closed.  

## Metrics

Note: this is currently a working list of all locations where we can currently gather contributor metrics. It is not *yet* the final set of metrics we will be using to monitor the success of the contributor program with.

### Bitergia dashboard
The [Bitergia dashboard](https://gitlab.biterg.io) is public and anyone will be able to use the dashboard (incl. view/filter/export/analyze the data).  There are a few administrative functionalities (e.g. getting short URL to share a saved dashboard for debugging) that require a login, and the login information is available in the Team Vault on 1Password.  There are recordings of Bitergia training and you can find them at the [Bitergia training livestream channel] (https://www.youtube.com/playlist?list=PL-gGdYiFOp7_9ij_wNLKyHgZsyhAY42rv).  

A number of custom dashboards have been created to look at merged community MRs, 1st time contributors, etc. To see the list of custom dashboards, click on `dashboard` on the upperleft (next to the Bitergia logo) and then click on the dashboard link from the list.  

### Quality dashboard
The Quality team publishes a [monthly dashboard](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit#gid=319422041) with the data update on the 4th Monday of each month.  The `Community MRs per release` tab shows merged MRs from the wider community.  

### GitLab.com
You can also directly query data from `Merge Requests` pages for projects (e.g. CE, EE, Gitter, Omnibus, Shell, etc.) on gitlab.com and aplly appropriate filters for milestone, labels, etc. Some of the examples are listed in the metrics table below.  

### Metrics table

NOTE: "GitLabbers included" column
- Yes (✓) means the metric includes contributions or contributors that contain both GitLab team members and the wider community
- No (✖) means the metric includes contributions or contributors that contain only the wider community

| METRIC | TIME SERIES | PROJECT | DATA SOURCE | GITLABBERS INCLUDED? | NOTES |
| - | - | - | - | - | - |
| <multiple> | Release | GitLab CE/EE | [Bitergia dashboard](https://gitlab.biterg.io) | ✖ | - |
| No. of merged contributions | Release | GitLab CE | [Quality dashboard](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit#gid=319422041) | ✖ | To be phased out in favour of the Bitergia Dashboard |
| No. of contributors | All time | GitLab CE | [GitLab contributors app](https://contributors.gitlab.com) | ✓ | - |
| No. of merged contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab organization | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab organization | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution) | ✖ | - |
| No. of WIP open contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&wip=no&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab EE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab EE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution) | ✖ | - |

## Projects

We generally gather data from the GitLab CE & EE projects. However, there are other projects contributors can submit merge requests to:

- https://gitlab.com/gitlab-org/
- https://gitlab.com/gitlab-org/gitlab-ee
- https://gitlab.com/gitlab-org/gitlab-ce
- https://gitlab.com/gitlab-org/gitlab-runner
- https://gitlab.com/gitlab-org/gitlab-shell
- https://gitlab.com/gitlab-org/omnibus-gitlab
- https://gitlab.com/gitlab-org/gitlab-pages

Additional projects:
- https://gitlab.com/gitlab-org/www-gitlab-com
- https://gitlab.com/gitlab-org/gollum-lib
